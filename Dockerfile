FROM debian:stable-slim AS download

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    curl \
    bzip2 \
    && t=https://www.zotero.org/download/ && \
    v=$( \
    curl -s $t -o - | \
    grep linux-x86_64 | \
    sed 's/.*linux-x86_64\":\"\([0-9.]*\)\".*/\1/' ) \
    && u="https://www.zotero.org/download/client/dl?channel=release&platform=linux-x86_64&version=" \
    && d=/tmp/zotero.tar.bz2 \
    && curl -L $u$v -o $d \
    && tar jxvf $d -C /opt \
    && rm $d


FROM download AS fleet

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    libgtk-3-0 \
    libx11-xcb1 \
    libdbus-glib-1-2 \
    libxt6
